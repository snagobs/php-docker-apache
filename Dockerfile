FROM php:7.2-apache

LABEL maintainer="snachosov <snagobs@gmail.com>"

RUN apt-get update && \
    docker-php-ext-install pdo pdo_mysql && \
    apt-get clean

COPY src/ /var/www/html/

EXPOSE $PORT